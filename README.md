# dotfiles

Configuration files that i've created to be used usually with linux distros

screenshots of the tokyo night rice as of November 10th, 2023:
![nvim1](/screenshots/nvim1.png)
![fetch](/screenshots/fetch.png)
![nvim2](/screenshots/nvim2.png)
![spt](/screenshots/spt.png)
![weechat](/screenshots/weechat.png)
![discord](/screenshots/discord.png)
![librewolf-dolphin](/screenshots/librewolf-dolphin.png)

for me:
set colemak layout for sddm:

`sudo localectl set-x11-keymap --no-convert "us" "pc105" "colemak"`
